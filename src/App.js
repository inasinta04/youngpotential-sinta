import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import  ListCategory from './Components/ListCategory/ListCategory';
import ListProduct from './Components/ListProduct/ListProduct';
import ProductDetail from './Components/ProductDetail/ProductDetail';

function App() {
  return (
    <Router>
      <div>
        <Routes>
          <Route path="/" element={<ListCategory />} />
          <Route path="/ListCategory" element={<ListCategory/> } />
          <Route path="/ListProduct" element={<ListProduct/> } />
          <Route path="/ListProduct/:category" element={<ListProduct/> } />
          <Route path="/ProductDetail" element={<ProductDetail/> } />
          <Route path="/ProductDetail/:detail" element={<ProductDetail/> } />
        </Routes>
      </div>
    </Router>

  );
}

export default App;
