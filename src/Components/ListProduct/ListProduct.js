import React from 'react';
import './ListProduct.css'
import Appbar from './Appbar/Appbar';
import Search from './Search/Search';
import ProductMerchant from './ProductMerchant/ProductMerchant';

function ListProduk() {
    return (
        <div className='background'>
            <div className='main'>
                <Appbar />
                <Search />
                <ProductMerchant/>  
            </div>
        </div>
    )
}

export default ListProduk;