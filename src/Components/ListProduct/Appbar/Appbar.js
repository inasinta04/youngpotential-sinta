import React, { useState, useEffect } from 'react';
import './Appbar.css';
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

function Appbar() {
    let navigate = useNavigate();

    const { category } = useParams();
    const [title, setTitle] = useState([])

    useEffect(() => {
        axios
            .get(`https://dummyjson.com/products/category/${category}`)
            .then(res => {
                console.log(res)
                setTitle(res.data.products)
            })
            .catch(err => {
                console.log(err)
            })
    }, [])
    console.log(title);
    return (
        <div className='header'>
            <div className='listProductBack'>
                <ArrowBackIcon className='backArrow' onClick={() => { navigate(`/ListCategory`) }} />
                {category}
            </div>

        </div>
    )
}

export default Appbar;