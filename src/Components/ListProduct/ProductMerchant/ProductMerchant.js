import React, { useState, useEffect } from "react";
import './ProductMerchant.css'
import axios from "axios";
import { useParams, useNavigate } from "react-router-dom";

function ProductMerchant() {
    let navigate = useNavigate();

    const { category } = useParams();

    const [list, setList] = useState([])
    useEffect(() => {
        axios
            .get(`https://dummyjson.com/products/category/${category}`)
            .then(res => {
                console.log(res)
                setList(res.data.products)
            })
            .catch(err => {
                console.log(err)
            })
    }, [])

    console.log(list)

    return (
        <div className="productMerchant">
            {
                list?.map((list, index) => (
                    <div className="productMerchantMain" key={index} onClick={() => { navigate(`/ProductDetail/${list.id}`) }}>
                        <img src={list.thumbnail} />
                        <p>{list.title}</p>
                        <b>${list.price}</b>
                        <button>Tambah</button> 
                    </div>
                   
                ))}
            
        </div>
    )
}

export default ProductMerchant;