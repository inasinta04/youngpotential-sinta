import React from "react";
import './Search.css'

import FilterAltOutlinedIcon from '@mui/icons-material/FilterAltOutlined';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';

import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';


// import { ThemeProvider } from '@material-ui/styles';

import {
    withStyles,
    makeStyles,
    Theme,

} from '@material-ui/core/styles';



const useStyles = makeStyles({
    
    Search: {
        width: '290px',
        [`& fieldset`]: {
            borderRadius: "100px",
          },
        ['&.Mui-focused fieldset'] : {
            border: "2px solid black",               
        }
        
        
    },

   
})

function Search() {
    const classes = useStyles();
    return (
        <div className="search">
            <div>
                <TextField
                    className={classes.Search}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <SearchOutlinedIcon />
                            </InputAdornment>
                        ),

                        style: {
                            height : "41px",
                            color: "#7A7A7A"
                          },
                    }}
                />
            </div>
            <div className="filterIcon">
                <FilterAltOutlinedIcon />
            </div>

        </div>
    )
}

export default Search;