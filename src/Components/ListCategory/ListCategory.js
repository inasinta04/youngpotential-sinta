import React from 'react';
import './ListCategory.css'
import Appbar from './Appbar/Appbar';
import Store from './Store/Store';
import ProductCategory from './ProductCategory/ProductCategory';
import { Link } from 'react-router-dom';


function ListCategory() {
    return (
        <div className='background'>
            <div className='main'>
                <Appbar />
                <Store />
                <div className='line' />
                <ProductCategory />
                <div className='favoriteBtn'>
                    <button>Jadikan Toko Favorit</button>
                </div>
            </div>
        </div>
    )
}

export default ListCategory;