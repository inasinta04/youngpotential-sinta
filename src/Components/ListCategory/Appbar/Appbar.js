import React from "react";
import './Appbar.css'
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import SearchIcon from '@mui/icons-material/Search';

function Header() {
    return (
        <div className="header">
            <div className="detail">
                <ArrowBackIcon className="backArrow" />
                <p>Detail Toko</p>
            </div>
            <SearchIcon />
        </div>
    )
}

export default Header;