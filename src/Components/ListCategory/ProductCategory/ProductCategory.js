import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import './ProductCategory.css';
import axios from "axios";

import smartphones from '../ProductCategoryImg/smartphone.png';
import laptop from '../ProductCategoryImg/laptop.png';
import fragrances from '../ProductCategoryImg/fragrances.png';
import groceries from '../ProductCategoryImg/groceries.png';
import homeDecoration from '../ProductCategoryImg/home-decoration.png';
import furniture from '../ProductCategoryImg/furniture.png';
import tops from '../ProductCategoryImg/tops.png';
import womenDress from '../ProductCategoryImg/women-dress.png';
import womenShoes from '../ProductCategoryImg/women-shoes.png';
import tshirt from '../ProductCategoryImg/tshirt.png';
import manShoes from '../ProductCategoryImg/man-shoes.png';
import manWatch from '../ProductCategoryImg/man-watch.png';
import womenWatch from '../ProductCategoryImg/women-watch.png';
import womenBags from '../ProductCategoryImg/women-bags.png';
import womenJewellery from '../ProductCategoryImg/women-jewellery.png';
import sunglasses from '../ProductCategoryImg/sunglasses.png';
import automotive from '../ProductCategoryImg/automotive.png';
import motorcycle from '../ProductCategoryImg/motorcycle.png';
import lighting from '../ProductCategoryImg/lighting.png';
import skincare from '../ProductCategoryImg/skincare.png';

function ProductCategory() {
    const Icon = ({ img }) => {
        if (img === "smartphones") return <img src={smartphones} />
        if (img === "laptops") return <img src={laptop} />
        if (img === "fragrances") return <img src={fragrances} />
        if (img === "skincare") return <img src={skincare} />
        if (img === "groceries") return <img src={groceries} />
        if (img === "home-decoration") return <img src={homeDecoration} />
        if (img === "furniture") return <img src={furniture} />
        if (img === "tops") return <img src={tops} />
        if (img === "womens-dresses") return <img src={womenDress} />
        if (img === "womens-shoes") return <img src={womenShoes} />
        if (img === "mens-shirts") return <img src={tshirt} />
        if (img === "mens-shoes") return <img src={manShoes} />
        if (img === "mens-watches") return <img src={manWatch} />
        if (img === "womens-watches") return <img src={womenWatch} />
        if (img === "womens-bags") return <img src={womenBags} />
        if (img === "womens-jewellery") return <img src={womenJewellery} />
        if (img === "sunglasses") return <img src={sunglasses} />
        if (img === "automotive") return <img src={automotive} />
        if (img === "motorcycle") return <img src={motorcycle} />
        if (img === "lighting") return <img src={lighting} />

    }

    let navigate = useNavigate();

    const [categories, setCategories] = useState([])

    useEffect(() => {
        axios
            .get('https://dummyjson.com/products/categories')
            .then(res => {
                console.log(res)
                setCategories(res.data)
            })
            .catch(err => {
                console.log(err)
            })
    }, [])

    console.log(categories);

    return (
        <div className="listCategory">
            <p className="listCategoryHeader">Kategori Produk</p>
            <div className="listCategoryMain" >
                {
                    categories.map(post => (
                        <div className="allCategory" onClick={() => { navigate(`/ListProduct/${post}`) }}>
                            <div className="imgWrapper">
                                <Icon img={post} />
                               
                            </div>
                            <p>{post}</p> 
                        </div>

                    ))}

            </div>


        </div>
    )
}

export default ProductCategory;