import React from "react";
import './Store.css';
import img1 from './img1.jpg';
import { makeStyles } from '@material-ui/core';

import FavoriteIcon from '@mui/icons-material/Favorite';

const useStyles = makeStyles({
    FavoriteIcon: {
        width: "14px !important",
        height: "14px !important",

    }
})

function Store() {
    const classes = useStyles();
    return (
        <div className="store-wrap">
            <div className="store">
                <img className="img-store" src={img1} />
                <div className="rating">
                    <FavoriteIcon className={classes.FavoriteIcon} />
                    <p>5.0</p>
                </div>
            </div>
            <div className="detailStore">
                <p className="storeName">Toko Semesta</p>
                <p className="storeStreet">Jl. Kasipah Raya No. 182, Jatingaleh, Kec. Candisari, Kota Semarang</p>
                <div className="storeBtn">
                    <button className="express">Express</button>
                    <button>09.00</button>
                    <button>16.00</button>
                </div>
            </div>
        </div>      
    )
}

export default Store;