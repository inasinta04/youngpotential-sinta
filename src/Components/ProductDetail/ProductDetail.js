import React, { useState, useEffect } from "react";
import './ProductDetail.css'
import axios from "axios";
import { useParams, useNavigate } from "react-router-dom";
import Divider from '@mui/material/Divider';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

function ProductDetail() {
    let navigate = useNavigate();

    const { detail } = useParams();

    const [singleProduct, setSingleProduct] = useState([])
    useEffect(() => {
        axios
            .get(`https://dummyjson.com/products/${detail}`)
            .then(res => {
                console.log(res.data)
                setSingleProduct(res.data)
            })
            .catch(err => {
                console.log(err)
            })
    }, [])
   

    return (
        <div className="background">
            <div className="main">
                <div className="header">
                    <div className="detail">
                        <ArrowBackIcon className="backArrow" onClick={() => { navigate(-1) }} />
                        {singleProduct.title}
                    </div>
                </div>
                <div className="singleWrap">
                    <img src={singleProduct.thumbnail} />
                    <div className="singleCategory">
                        {singleProduct.category}
                    </div>
                    <div className="singleTitle">
                        <p>{singleProduct.title}</p>
                        <p>${singleProduct.price}</p>
                    </div>
                    <Divider />
                    <div className="singleDescription">
                        <span>Deskripsi Produk</span>
                        <p>{singleProduct.description}</p>
                    </div>
                    <button className="addProduct">
                        Tambah Pesanan
                    </button>
                </div>
            </div>
        </div>
    )
}

export default ProductDetail;